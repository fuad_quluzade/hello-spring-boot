package az.ingress.dto;

import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Timestamp;
import java.time.LocalDate;

@Data
public class StudentDto {
    private Long id;
    private String name;
    private String institute;
    private LocalDate birthday;

    private Integer generation;


    private Double salary;
    @Enumerated(EnumType.STRING)
//    private Size size;
    private Byte weekDays;

    private Timestamp addDate;
}
