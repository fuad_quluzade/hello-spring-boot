package az.ingress.repository;

import az.ingress.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<PhoneNumber,Long> {
}
