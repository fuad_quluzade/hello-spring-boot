package az.ingress.repository;

import az.ingress.entity.Student;
import liquibase.repackaged.org.apache.commons.lang3.builder.ToStringExclude;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query(name = "getAgeGreaterThan")
    List<Student> findAllByGenerationGreaterThan(int generation);

    //native
//    @Query(value = "select s.id as id,s.add_date,s.birthday,s.institute,s.generation,s.salary,s.week_days,s.name,s.last_name,pn.number from helloworld.student s join helloworld.phone_number pn on s.id =pn.student_id;",nativeQuery = true)
//    List<Student> getAll();
//jpql
    @Query(value = "select s from Student s left join fetch s.phoneNumber left join fetch s.adressess")
    List<Student> getAllByJpql();

    //    entity graph
//    @EntityGraph(value = "numbers")
//    List<Student> findAll();

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,attributePaths = {"phoneNumber"})
    List<Student> findAll();


}
