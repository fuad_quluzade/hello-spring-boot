package az.ingress.repository;

import az.ingress.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
@Repository
public interface AccountRepository extends JpaRepository<Account,Long>, JpaSpecificationExecutor<Account> {

//    Account findByIdAndVersion(Long id,Integer version);

    List<Account> findByBalanceGreaterThanAndNameStartsWith(Integer balance,String name);

    @Lock(LockModeType.OPTIMISTIC)
   Optional<Account> findByName(String name);


}
