package az.ingress.entity;

import az.ingress.enums.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.xml.bind.v2.TODO;
import liquibase.repackaged.org.apache.commons.lang3.builder.ToStringExclude;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@Table(name = "Student")
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({
        @NamedQuery(query = "select s from Student  s where s.generation> :generation", name = "getAgeGreaterThan")
})
@ToString
//@EqualsAndHashCode(exclude = {"phoneNumber,adressess"})
//@NamedEntityGraphs(value = {
//        @NamedEntityGraph(name = "numbers",
//                attributeNodes = @NamedAttributeNode("phoneNumber")),
//        @NamedEntityGraph(name = "address",
//                attributeNodes = @NamedAttributeNode("adressess")),
//        @NamedEntityGraph(name = "addressAndPhone",
//                attributeNodes = {
//                        @NamedAttributeNode("adressess"),
//                        @NamedAttributeNode("phoneNumber")
//                })
//})
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "last_name")
    private String lastName;
    private LocalDate birthday;
    private Integer generation;


    private Double salary;
    //    @Enumerated(EnumType.STRING)
//    private Size size;
    @Column(name = "week_days")
    private Byte weekDays;
    private String institute;

    @Column(name = "add_date")
    private Timestamp addDate;
    //
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "address_id")
//    Address address;
//
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    Set<PhoneNumber> phoneNumber;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    Set<Address> adressess;
//
//
//    @ManyToMany
//    @JoinTable(name = "students_roles",
//            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id "))
//    List<Role> roles;

}
