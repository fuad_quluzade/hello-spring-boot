package az.ingress.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "orders")
public class Order {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String customer;
    private float total;

    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    private Set<OrderDetail> detailSet=new HashSet<>();

    public Order() {
    }

    public Order(Integer id, String customer, float total) {
        this.id = id;
        this.customer = customer;
        this.total = total;
    }

    public Order(Order order) {
        this.id = order.getId();
        this.customer = order.getCustomer();
        this.total = order.getTotal();
        this.detailSet=order.getDetailSet();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Set<OrderDetail> getDetailSet() {
        return detailSet;
    }

    public void setDetailSet(Set<OrderDetail> detailSet) {
        this.detailSet = detailSet;
    }
}
