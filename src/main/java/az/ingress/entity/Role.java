package az.ingress.entity;


import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name = "Role")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}
