package az.ingress;

import az.ingress.entity.Account;
import az.ingress.entity.Car;
import az.ingress.repository.*;
import az.ingress.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
//@EnableCaching
@Slf4j
@EnableConfigurationProperties
public class HelloSpringBootApplication implements CommandLineRunner {

    private final AddressRepository addressRepository;
    private final StudentRepository studentRepository;
    private final PhoneRepository phoneRepository;
    private final RoleRepository roleRepository;
    private final OrderDetailsRepository orderDetailsRepository;
    private final AccountRepository accountRepository;
    private final TransferService transferService;


    public static void main(String[] args) {
        SpringApplication.run(HelloSpringBootApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Account from = accountRepository.findById(1L).get();
//        Account to = accountRepository.findById(1L).get();
//
//        transferService.transfer(from,to,40);

//        for (int i = 0; i < 200; i++) {
//            accountRepository.save(Account.builder()
//                    .name("Fuad " +i)
//                    .balance(50+1)
//                    .build());
//        }





//        List<Account> ali = accountRepository.findByBalanceGreaterThanAndNameStartsWith(75, "Ali");
//        ali.forEach(System.out::println);

//
//        List<Account> list=new ArrayList<>();
//        list.add(account);
//        list.add(account1);


//        accountRepository.saveAll(List.of(account,account1));
//        accountRepository.saveAll(list);

//        Account from=accountRepository.findById(1L).get();
//       Account to=accountRepository.findById(2L).get();

//       transferService.transfer1(from,to,100);
//        transferService.transfer2(30);

//        Account byid=accountRepository.getById(4L);
//        System.out.println(byid.getName());


    }


}


//		Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/helloworld","root","root");
//		Statement statement=con.createStatement();
//		ResultSet resultSet=statement.executeQuery("select * from student");
//
//		while (resultSet.next()){
//			System.out.println(resultSet.getInt(1));
//			System.out.println(resultSet.getString(2));
//			System.out.println(resultSet.getString(3));
//		}

//		EntityManagerFactory em= Persistence.createEntityManagerFactory("PERSISTENCE");
//		EntityManager entityManager = em.createEntityManager();
//		entityManager.getTransaction().begin();
//		entityManager.merge(Student.builder().name("Namiq").lastName("Ibrahimli").build());
//		entityManager.getTransaction().commit();
//		entityManager.close();

//		Address address=Address.builder()
//				.name("Baku")
//				.build();
//

//        for (int i=0;i<10;i++) {
//            Student student = Student.builder()
//                    .name("Kamran "+i)
//                    .lastName("Anyone " + i)
//                    .generation(i)
//                    .build();
////
////		addressRepository.save(address);
////            studentRepository.save(student);
//            PhoneNumber phoneNumber1 = PhoneNumber.builder()
//                    .student(student)
//                    .number("944"+i)
//                    .build();
//            PhoneNumber phoneNumber2 = PhoneNumber.builder()
//                    .student(student)
//                    .number("5555")
//                    .build();
//            List<PhoneNumber> list = new ArrayList<>();
////         List<PhoneNumber> list = List.of(phoneNumber1, phoneNumber2, phoneNumber3);
//            list.add(phoneNumber1);
//            list.add(phoneNumber2);
//            phoneRepository.saveAll(list);
//        }
//
//        student.setPhoneNumber(list);
//        studentRepository.save(student);

//        Student student = studentRepository.findById(2L).get();
//
//        Role admin=Role.builder()
//                .name("ADMIN")
//                .build();
//
//        Role user=Role.builder()
//                .name("USER")
//                .build();
//
//        roleRepository.save(admin);
//        roleRepository.save(user);
//
//        List<Role> roles=new ArrayList<>();
//        roles.add(admin);
//        roles.add(user);
//        student.setRoles(roles);
//        studentRepository.save(student);
//
//        Student student1 = studentRepository.findById(1L).get();
//        List<Role> roles1=new ArrayList<>();
//        roles1.add(user);
//         student1.setRoles(roles1);
//         studentRepository.save(student1);
//           Order order=new Order();
//           order.setCustomer("Fuad");
//           order.setTotal(1.2F);
//
//           Product product=new Product();
//           product.setName("qelem");
//           product.setPrice(1.3F);
//
//           OrderDetail orderDetail=new OrderDetail();
//           orderDetail.setProduct(product);
//           orderDetail.setOrder(order);
//           orderDetail.setActivated(true);
//           orderDetail.setRegisteredDate(new Date());
//
//
//           orderDetailsRepository.save(orderDetail);


//   for (int i=0;i<30;i++) {
//       Student b = new Student();
//       b.setName("Resad " + i);
//       b.setLastName("Quluzade "+i);
//       b.setGeneration(26 +i );
//       studentRepository.save(b);
//
//   }


//        studentRepository.findAll().forEach(System.out::println);

//          studentRepository.getAllByJpql().forEach(System.out::println);

//        studentRepository.findAll().forEach(System.out::println );

