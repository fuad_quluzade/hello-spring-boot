package az.ingress.service;

import az.ingress.dto.StudentDto;
import az.ingress.entity.Student;
import az.ingress.repository.AccountRepository;
import az.ingress.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;
private final AccountRepository accountRepository;
//    public List<StudentDto> getStudents() {
//        val students = studentRepository.findAll()
//                .stream()
//                .map(StudentMapper.INSTANCE::studentToDto)
//                .collect(Collectors.toList());
//        return students;
//    }

    public List<StudentDto> getStudents(){
             val student =studentRepository.findAll();
            return convertToBookDTOList(student);
    }

    public void addStudent(StudentDto studentDto) {
        if (studentDto.getId() != null) {
            Optional<Student> studentById = studentRepository.findById(studentDto.getId());
            studentById.ifPresent(student -> {
                throw new RuntimeException("Student is present");
            });
        }
        Student student=modelMapper.map(studentDto,Student.class);
        System.out.println(student);
        studentRepository.save(student);
    }

    public void updateStudent(Long id,StudentDto studentDto) {
        Student student=studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student with id:" + id + " is not found."));
        student.setId(id);
        student.setName(studentDto.getName());
//        student.setInstitute(studentDto.getInstitute());
//        student.setBirthday(studentDto.getBirthday());
        studentRepository.save(student);
    }


    private List<StudentDto> convertToBookDTOList(List<Student> students) {
        return students.stream()
                .map(book -> modelMapper.map(book, StudentDto.class))
                .collect(Collectors.toList());
    }


    public void deleteStudent(Long studentId) {
        if(studentId ==null){
            throw new RuntimeException("Student with id:" + studentId + " is not found.");
        }
        studentRepository.deleteById(studentId);
    }
//
//    @Transactional
//    public void transfer1(Integer amount) throws Exception{
//        var from =accountRepository.findById(1L).get();
//        var to=accountRepository.findById(2L).get();
//        log.info("trying transfer money");
//        if (from.getBalance() < amount) {
//            throw new RuntimeException("not sufficient balance");
//        }
//
//        from.setBalance(from.getBalance() - amount);
//        to.setBalance(to.getBalance() + amount);
//
//        accountRepository.save(from);
//        if (true) {
//            throw new Exception();
//        }
//        accountRepository.save(to);
//    }

}
