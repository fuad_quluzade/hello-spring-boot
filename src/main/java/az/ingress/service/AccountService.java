package az.ingress.service;

import az.ingress.entity.Account;
import az.ingress.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory emf;
//    private final RedisTemplate<String,String> redisTemplate;
//    private final CacheManager cacheManager;
//
//    @Cacheable(cacheNames = "accounts" ,key = "#s")
//    public Account findByName(String s) {
//        return accountRepository.findByName(s).get();
//    }
//
//
////    @Cacheable(cacheNames = "accounts" )
////    public List<Account> findAll() {
////        return accountRepository.findAll();
////    }
//
//    @Cacheable(cacheNames = "accounts" )
//    public List<Account> findAll() {
//        Cache cache = cacheManager.getCache("accounts");
//        Class<List<Account>> accClass=(Class) List.class;
//        List<Account> acc= cache.get("all",accClass);
//        if(acc ==null){
//            acc=accountRepository.findAll();
//            cache.put("all",acc);
//        }
//        return acc;
//    }
//
//    public void create() {
//        accountRepository.save(Account.builder()
//                .name("fuad")
//                .balance(5)
//                .build());
//        Cache cache = cacheManager.getCache("accounts");
//        cache.evict("all");
//
//    }

    @SneakyThrows
    public Account getById(Long id) {
//        return accountRepository.findById(id).orElseThrow(()->new RuntimeException("account not found"));
        EntityManager entityManager = emf.createEntityManager();
        Account account = entityManager.find(Account.class, 1L);
        Thread.sleep(10);
        entityManager.close();
        return account;
    }

//    @Transactional
    public void transfer1(Integer amount) throws Exception{
        var from =accountRepository.findById(1L).get();
        var to=accountRepository.findById(2L).get();
        System.out.println(from);
        System.out.println(to);
        log.info("trying transfer money");
        if (from.getBalance() < amount) {
            throw new RuntimeException("not sufficient balance");
        }

        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);

        accountRepository.save(from);
        if (true) {
            throw new RuntimeException();
        }
        accountRepository.save(to);
    }
}
