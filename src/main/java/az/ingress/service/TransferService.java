package az.ingress.service;

import az.ingress.entity.Account;
import az.ingress.repository.AccountRepository;
import az.ingress.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {
    private final AccountRepository accountRepository;
    private final StudentService studentService;

    // Account from=accountRepository.findByIdAndVersion(1L,1);
//       Account to=accountRepository.findByIdAndVersion(2L,1);
//    @Transactional
//    @SneakyThrows
//    public void transfer(Account from, Account to, Integer amount) {
//        log.info("trying transfer money");
//        if (from.getBalance() < amount) {
//            throw new RuntimeException("not sufficient balance");
//        }
//
////        log.info("waiting 10 second");
////        Thread.sleep(10000);
//
//        Optional<Account> byId = accountRepository.findById(from.getId());
//        if (byId.get().getVersion() == 1) {
//            log.info("working");
//            from.setBalance(from.getBalance() - amount);
//            from.setVersion(from.getVersion() + 1);
//            to.setBalance(to.getBalance() + amount);
//            accountRepository.save(from);
//            accountRepository.save(to);
//        }
//        log.info("not working");
//
//
//    }

//    public void transfer2(Integer amount) throws Exception{
//        studentService.transfer1(amount);
//    }


//        public void transfer(Account from, Account to, Integer amount) {
//        if (from.getBalance() < amount) {
//            throw new RuntimeException("not sufficient balance");
//        }
//
//
//            from.setBalance(from.getBalance() - amount);
//            to.setBalance(to.getBalance() + amount);
//            accountRepository.save(from);
//            if(true){
//                throw new RuntimeException();
//            }
//            accountRepository.save(to);



//    }








}

