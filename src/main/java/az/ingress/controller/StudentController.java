package az.ingress.controller;

import az.ingress.dto.StudentDto;
import az.ingress.entity.Account;
import az.ingress.entity.GenericSpecification;
import az.ingress.entity.SearchCriteria;
import az.ingress.repository.AccountRepository;
import az.ingress.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    private final AccountRepository accountRepository;

    @GetMapping
    public List<StudentDto> getStudents() {
        return studentService.getStudents();
    }

    @PostMapping("/add")
    public void addNewStudent(@RequestBody StudentDto studentDto) {
        studentService.addStudent(studentDto);
    }

    @PutMapping("/{id}")
    public void updateStudent(@PathVariable Long id, @RequestBody StudentDto studentDto) {
        studentService.updateStudent(id, studentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        studentService.deleteStudent(id);
    }


    @GetMapping("/account")
    public List<Account> getAccountByCriteria(@RequestBody  List<SearchCriteria> criteria){

        GenericSpecification<Account> genericSpecification = new GenericSpecification<>();
        genericSpecification.add(criteria);
       return accountRepository.findAll(genericSpecification);

    }
}
