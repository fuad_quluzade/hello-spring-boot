package az.ingress.controller;

import az.ingress.entity.Account;
import az.ingress.repository.AccountRepository;
import az.ingress.repository.StudentRepository;
import az.ingress.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/acc")
@Slf4j
public class AccountController {

    private final AccountRepository accountRepository;
    private final AccountService accountService;
    private final List<String> a=new ArrayList<>();
    private final StudentRepository studentRepository;

    @GetMapping
    public Account findById(){
        return accountRepository.findById(1L).get();
    }

//    @GetMapping("/name")
//    @Transactional
//    public Account findByName(){
//        Account  account= accountService.findByName("Murad");
//        return account;
//    }
//
//    @GetMapping("/all")
//    public List<Account> findAll(){
//       return accountService.findAll();
//
//    }
//
//    @PostMapping
//    public void create(){
//        accountService.create();
//    }
//
//
//    @GetMapping("/update")
//    public void read() throws Exception{
//        Account account=accountRepository.findByName("Murad").get();
//        account.setBalance(account.getBalance()+40);
//        log.info("account get sleep");
//        Thread.sleep(10000);
//        accountRepository.save(account);
//        log.info("account balance updated");
//    }

//    @GetMapping("/{id}")
////    @Transactional
//    public Account update(@PathVariable Long id) throws Exception{
//        log.info("trying to read");
//        Account account=accountRepository.findById(id).get();
////        Thread.sleep(100);
//        log.info("read success" + account);
//        return account;
//    }

    @GetMapping("/list")
    public List<Account> update() throws Exception{
        log.info("trying to read");
        List<Account> account=accountRepository.findAll();
        log.info("read success" + account);
        return account;
    }

    @GetMapping("/1")
    public Account getById(){
//        log.info("1");
      return  accountService.getById(2L);
//       return new Account();
    }

    @GetMapping("/2")
    public Account getById2() throws InterruptedException {
//        Thread.sleep(10);
//        log.info("1");
        return accountService.getById(1L);
//        return new Account();
    }

    @GetMapping("/student")
    public void student(){
        System.out.println(studentRepository.findById(1L).get());
    }

    @GetMapping("/transfer")
//    @SneakyThrows
    public void transfer() throws Exception {
       accountService.transfer1(40);
    }

//    @GetMapping("/tr")
//    public void transfer1(){
//        int amount=40;
//        Account from=accountRepository.findById(1L).get();
//        if(from.getBalance()<amount){
//            throw new RuntimeException("influence  balance");
//        }
//        Account to=accountRepository.findById(2L).get();
//        from.setBalance(from.getBalance()-amount);
//        to.setBalance(to.getBalance()+amount);
//        accountRepository.save(from);
//        accountRepository.save(to);
//
//    }

}
