package az.ingress.controller;

import az.ingress.dto.PublisherDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/kafka")
@Slf4j
public class PublisherController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping("/send")
    public void publish(@RequestParam String message) {
        PublisherDto publisherDto=PublisherDto.builder()
                .id(1L)
                .name("Fuad")
                .lastName("Quluzade")
                .email("test@mail.ru")
                .age(28)
                .build();
        ProducerRecord<String,String> record = new ProducerRecord<String,String>("ms-demo-topic", message);
        record.headers().add("Accept-Language","en".getBytes());
        kafkaTemplate.send(record);
    }
}
