package az.ingress.controller;

import az.ingress.entity.Account;
import az.ingress.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
@Slf4j
public class TransferController {

    private final AccountRepository accountRepository;

//    @GetMapping
//    @SneakyThrows
//    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void transfer(){
        int amount=40;
        Account from=accountRepository.findById(1L).get();
        if(from.getBalance()<amount){
            throw new RuntimeException("influence  balance");
        }
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Account to=accountRepository.findById(2L).get();
        from.setBalance(from.getBalance()-amount);
        to.setBalance(to.getBalance()+amount);
        accountRepository.save(from);
        accountRepository.save(to);

    }

    @GetMapping("/update")
    @Transactional
    public void read() throws Exception{
        Account account=accountRepository.findByName("Murad").get();
        account.setBalance(account.getBalance()+40);
        log.info("account get sleep");
        Thread.sleep(10000);
        accountRepository.save(account);
        log.info("account balance updated");
    }

    @GetMapping("/read")
    @Transactional
    public Account update() throws Exception{
        Account account=accountRepository.findByName("Murad").get();
        return account;
    }
}
