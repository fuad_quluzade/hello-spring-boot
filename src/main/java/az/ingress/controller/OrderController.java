//package az.ingress.controller;
//
//import az.ingress.entity.Order;
////import az.ingress.entity.Order$;
//
////import az.ingress.entity.Order$;
////import com.speedment.jpastreamer.application.JPAStreamer;
//import lombok.RequiredArgsConstructor;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/order")
//public class OrderController {
//
//    private final JPAStreamer streamer;
//
//    @GetMapping
//    public List<Order> orderList() {
//        return streamer.stream(Order.class)
//                .collect(Collectors.toList());
//    }
//
//    @GetMapping("/greater-than")
//    public List<Order> idGreaterThan(@RequestParam Integer id) {
//        return streamer.stream(Order.class)
//                .filter(Order$.id.greaterThan(id))
//                .map(Order::new)
//                .collect(Collectors.toList());
//    }
//
//    @GetMapping("/offset/{offset}/limit/{limit}")
//    public List<Order> findAllWithPagination(@PathVariable("offset") int offset,
//                                             @PathVariable("limit") int limit) {
//        return streamer.stream(Order.class)
//                .skip(offset)
//                .limit(limit)
//                .map(Order::new)
//                .collect(Collectors.toList());
//
//    }
//
//    @GetMapping("/{id}/count-orders")
//    public long findById(@PathVariable Integer id) {
//        return streamer.stream(Order.class)
//                .filter(Order$.id.equal(id))
//                .map(Order::getDetailSet)
//                .mapToLong(Set::size)
//                .sum();
//
//    }
//}
