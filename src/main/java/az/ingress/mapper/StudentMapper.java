package az.ingress.mapper;

import az.ingress.dto.StudentDto;
import az.ingress.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class StudentMapper {

    public static final StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    public abstract Student dtoToEntity(StudentDto dto);

    public abstract StudentDto studentToDto(Student student);
}
